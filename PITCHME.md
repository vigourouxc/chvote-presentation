---
## CHVote 2.0
![Header](assets/img/header.jpg)
## Protocole distribué, modèle de sécurité et architecture

---
## Sommaire

* Préambule et contexte
* Modèle de sécurité
* Architecture générale de la solution
* Architecture distribuée du _protocol core_
* Modèle de menace du _protocol core_
* Le futur
* Rétrospective

---
![Header](assets/img/background.jpg)
## Préambule et contexte

+++

@snap[west span-30]
## Bio
@snapend

@snap[east span-70]
@ul
* Architecte IT sur projets e-vote de Genève depuis 2010.
* Design, développement, documentation, opérations
* Expertise sécurité analyse et design
* Team développement protocole de vote
@ulend
@snapend

---
* Préambule
    * Qui suis-je
    * Historique du projet
    * Etat actuel
    * Contraintes légales et projet
* Modèle de sécurité
    * Objectifs de sécurité
    * Exigences principales
    * Modèle de confiance
    * Limitations
        * Sécurité du poste client: menaces et acceptation
        * Identité numérique: authentification uniquement de la carte de vote
* Architecture
    * Le protocole comme composant indépendant
    * Les applications de support
    * Vue d'ensemble
    * Choix d'implémentation
* Architecture distribuée
    * Objectifs
    * Explication du principe des composants de contrôle
        * Métaphore bulletin copié dans plusieurs urnes
    * Principes
        * Event driven
        * At least once delivery, idempotence
        * Vision initiale: livraison garantie par le middleware
        * Vision finale: Modèle d'acteur
        * Choix d'implémentation
* Modèle de menace
    * Principe d'analyse
    * Modèle de menace du composant protocole
* Futur
    * Publication du projet sur Gitlab
    * Reprise du projet ? Opportunités, menaces
    * Améliorations (point de vue personnel)
        * Composants de contrôle indépendants
            * Exploitation
                * Organisations indépendantes: financièrement, politiquement, hiérarchiquement, technologiquement
            * Implémentations indépendantes
                * langage: java/go/ruby/rust... performances!!
                * persistance: postgresql, mongodb..
        * Identité numérique
* Ce qui aurait pu être mieux fait
    * SSDLC
    * Gestion des vulnérabilités dans le CI
    * Tests de sécurité


---
